<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Regex;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'label' => 'Your firstname',
                'attr' => [
                    'placeholder' => 'Please type your firstname here'
                ],
                'constraints' => [
                    new Length([
                        'min' => 2,
                        'max' => 30,
                    ])
                ]
            ])
            ->add('lastname', TextType::class, [
                'label' => 'Your lastname',
                'attr' => [
                    'placeholder' => 'Please type your lastname here'
                ],
                'constraints' => [
                    new Length([
                        'min' => 2,
                        'max' => 30,
                    ])
                ]
            ])
            ->add('email', EmailType::class, [
                'label' => 'Your Email',
                'attr' => [
                    'placeholder' => 'Please type your email here'
                ],
                'constraints' => [
                    new Length([
                        'min' => 2,
                        'max' => 60,
                    ])
                ]
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'The password and confirmation have to be the same.',
                'required' => true,
                'first_options' => [
                    'label' => 'Your password',
                    'attr' => [
                        'placeholder' => 'Please type your password here'
                    ]
                ],
                'second_options' => [
                    'label' => 'Confirm password',
                    'attr' => [
                        'placeholder' => 'Please confirm your password here'
                    ]
                ],
                'constraints' => [
                    new Regex([
                        'pattern' => '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#@$!%*?&])[A-Za-z\d#@$!%*?&]{6,30}$/',
                        'match' => true,
                        'message' => 'Your password must have a minimum 6 and maximum 30 characters,
                        at least one uppercase letter, one lowercase letter, one number and one special character',
                    ])
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Sign Up'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
